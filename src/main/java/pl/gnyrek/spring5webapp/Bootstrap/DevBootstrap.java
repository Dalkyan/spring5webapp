package pl.gnyrek.spring5webapp.bootstrap;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import pl.gnyrek.spring5webapp.model.Author;
import pl.gnyrek.spring5webapp.model.Book;
import pl.gnyrek.spring5webapp.model.Publisher;
import pl.gnyrek.spring5webapp.repositories.AuthorRepository;
import pl.gnyrek.spring5webapp.repositories.BookRepository;
import pl.gnyrek.spring5webapp.repositories.PublisherRepository;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {


    private AuthorRepository authorRepository;
    private BookRepository bookRepository;
    private PublisherRepository publisherRepository;

    public DevBootstrap(AuthorRepository authorRepository, BookRepository bookRepository, PublisherRepository publisherRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
        this.publisherRepository = publisherRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initData();
    }

    private void initData() {

        //publishers
        Publisher publisher1 = new Publisher();
        publisher1.setName("German Library");
        Publisher publisher2 = new Publisher();
        publisher2.setName("Wydawnictwo Czarne");
        Publisher publisher3 = new Publisher();
        publisher3.setName("Księgarnia Akademicka");
        Publisher publisher4 = new Publisher();
        publisher4.setName("Wydawnictwo HELION");
        publisherRepository.save(publisher1);
        publisherRepository.save(publisher2);
        publisherRepository.save(publisher3);
        publisherRepository.save(publisher4);

        //Hesse
        Author hermann = new Author("Hermann", "Hesse");
        Book steppenWolf = new Book("Steppenwolf", "12453", publisher1);
        hermann.getBooks().add(steppenWolf);
        steppenWolf.getAuthors().add(hermann);
        authorRepository.save(hermann);
        bookRepository.save(steppenWolf);

        //nietzsche
        Author nietzsche = new Author("Friedrich", "Nietzsche");
        Book zaratusthra = new Book("Also sprach Zarathustra", "63423", publisher1);
        Book ecceHomo = new Book("Ecce homo", "63753", publisher1);
        Book wanderer = new Book("Der Wanderer und Sein Schatten", "637564", publisher1);
        nietzsche.getBooks().add(zaratusthra);
        nietzsche.getBooks().add(ecceHomo);
        nietzsche.getBooks().add(wanderer);
        zaratusthra.getAuthors().add(nietzsche);
        ecceHomo.getAuthors().add(nietzsche);
        wanderer.getAuthors().add(nietzsche);
        authorRepository.save(nietzsche);
        bookRepository.save(zaratusthra);
        bookRepository.save(ecceHomo);
        bookRepository.save(wanderer);


        //baranczak
        Author baranczak = new Author("Stanisław", "Barańczak");
        Book pegaz = new Book("Pegaz zdębiał", "09876", publisher2);
        baranczak.getBooks().add(pegaz);
        pegaz.getAuthors().add(baranczak);
        authorRepository.save(baranczak);
        bookRepository.save(pegaz);

        //javax
        Author horstmann = new Author("Cay S.", "Horstmann");
        Book javaX = new Book("Java. Podstawy", "978-83-283-2480-0", publisher4);
        horstmann.getBooks().add(javaX);
        javaX.getAuthors().add(horstmann);
        authorRepository.save(horstmann);
        bookRepository.save(javaX);


        //kazak
        Author gulayhan = new Author("Gülayhan", "Aqtay");
        Author jankowski = new Author("Henryk", "Jankowski");
        Book slownikKazak = new Book("Słownik kazachsko-polski", "978-83-7638-079-7", publisher3);
        gulayhan.getBooks().add(slownikKazak);
        authorRepository.save(gulayhan);
        jankowski.getBooks().add(slownikKazak);
        authorRepository.save(jankowski);
        slownikKazak.getAuthors().add(gulayhan);
        slownikKazak.getAuthors().add(jankowski);
        bookRepository.save(slownikKazak);

        publisher1.getBooks().add(steppenWolf);
        publisher1.getBooks().add(zaratusthra);
        publisher1.getBooks().add(ecceHomo);
        publisher2.getBooks().add(pegaz);
        publisher3.getBooks().add(slownikKazak);
        publisher4.getBooks().add(javaX);

        publisherRepository.save(publisher1);
        publisherRepository.save(publisher2);
        publisherRepository.save(publisher3);
        publisherRepository.save(publisher4);
    }
}
